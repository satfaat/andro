
- https://github.com/android/codelab-android-compose
- https://github.com/android/compose-samples/tree/master/JetNews
- https://m2.material.io/design/material-studies/crane.html


## docu

- https://developer.android.com/develop/ui/compose/state?authuser=1#state-in-composables
- https://kotlinlang.org/docs/delegated-properties.html#standard-delegates
- https://developer.android.com/studio/debug/layout-inspector?authuser=1
- https://developer.android.com/develop/ui/compose/state?authuser=1#restore-ui-state
- https://developer.android.com/develop/ui/compose/architecture
- https://m3.material.io/