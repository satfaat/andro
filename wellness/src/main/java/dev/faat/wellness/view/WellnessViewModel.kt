package dev.faat.wellness.view

import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import dev.faat.wellness.dt.TaskDto
import dev.faat.wellness.dt.getWellnessTasks


class WellnessViewModel : ViewModel() {
    private val _tasks = getWellnessTasks().toMutableStateList()
    val tasks: List<TaskDto>
        get() = _tasks

    fun remove(item: TaskDto) {
        _tasks.remove(item)
    }

    fun changeTaskChecked(item: TaskDto, checked: Boolean) =
        _tasks.find { it.id == item.id }?.let { task ->
            task.checked = checked
        }
}