package dev.faat.wellness.view

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.dem.interfacelib.Btn2


@SuppressLint("UnrememberedMutableState")
@Composable
@Preview(showBackground = true, backgroundColor = 0xFFF5F0EE)
fun WaterCounter(modifier: Modifier = Modifier) {
    Column(modifier = modifier.padding(16.dp)) {
//        val count: MutableState<Int> = remember { mutableStateOf(0) }
        var count: Int by rememberSaveable { mutableStateOf(0) }

        if (count > 0) {
            var showTask by remember { mutableStateOf(true) }
            if (showTask) {
                WellnessTaskItem(
                    onClose = { showTask = false },
                    taskName = "Have you taken your 15 minute walk today?"
                )
            }
            Text("You've had ${count} glasses.")
        }
        Row(modifier = modifier.padding(top = 8.dp)) {
            ButtonCount({ count++ }, count < 10)
            //ClearCount
            Btn2(
                "Clear water count",
                { count = 0 },
                Modifier.padding(start = 8.dp)
            )
        }
    }
}


@Composable
fun ButtonCount(onClick: () -> Unit, enabled: Boolean) {
    Button(
        onClick = onClick,
        enabled = enabled
    ) { Text("Add one") }
}


@Composable
fun StatefulCounter(modifier: Modifier = Modifier) {
    var countState by rememberSaveable { mutableStateOf(0) }
    StatelessCounter(countState, { countState++ }, modifier)
}

@Composable
fun StatelessCounter(count: Int, onIncrement: () -> Unit, modifier: Modifier = Modifier) {
    Column(modifier = modifier.padding(16.dp)) {
        if (count > 0) {
            Text("You've had $count glasses.")
        }
        Button(onClick = onIncrement, Modifier.padding(top = 8.dp), enabled = count < 10) {
            Text("Add one")
        }
    }
}