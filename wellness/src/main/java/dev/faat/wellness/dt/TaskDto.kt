package dev.faat.wellness.dt

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

class TaskDto(
    val id: Int,
    val label: String,
    initialChecked: Boolean = false
) {
    var checked by mutableStateOf(initialChecked)
}

fun getWellnessTasks() = List(30) { i -> TaskDto(i, "Task # $i") }
