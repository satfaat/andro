package dev.faat.wellness

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import dev.faat.wellness.ui.theme.DemoTheme
import dev.faat.wellness.view.WellnessScreen


@Composable
@Preview(showBackground = true, backgroundColor = 0xFFF5F0EE)
fun WellnessApp(modifier: Modifier = Modifier) {
    DemoTheme {
        Surface(
            modifier = modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) { WellnessScreen() }
    }
}