pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Demo"
include(
    ":app",
    ":tiptime",
    ":unscramble",
    ":compose1",
    ":other",
    ":syntax",
    ":composenavdem",
    ":compose2",
    ":wellness",
    ":sunflower",
    ":crane"
)
include(":cupcake")
include(":InterfaceLib")
include(":ktorclient")
