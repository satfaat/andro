package dev.faat.tiptime

import androidx.compose.ui.test.junit4.createComposeRule
import dev.faat.tiptime.ui.theme.DemoTheme
import org.junit.Rule
import org.junit.Test
import java.text.NumberFormat

class TipUiPomTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun calculate_20_percent_tip() {
        composeTestRule.setContent {
            DemoTheme {
                dev.faat.tiptime.screen.TipTimeLayout()
            }
        }

        val tipTimeScreen = dev.faat.tiptime.TipTimeScreen(composeTestRule)
        val expectedTip = NumberFormat.getCurrencyInstance().format(2)

        tipTimeScreen
            .enterBillAmount("10")
            .enterTipPercentage("20")
            .assertTipAmountDisplayed(expectedTip)
    }
}