package dev.faat.tiptime

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule
import org.junit.Test
import java.text.NumberFormat


class EspressoDemoTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun listGoesOverTheFold() {
        onView(withText("Hello world"))
            .check(matches(isDisplayed()))
    }

    @Test
    @Throws(InterruptedException::class)
    fun calculate_20_percent_tip() {
        // Enter bill amount
        onView(withText("Bill Amount"))
            .perform(typeText("10"))

        // Enter tip percentage
        onView(withText("Tip Percentage"))
            .perform(typeText("20"))

        // Wait for calculations to update (optional)
        Thread.sleep(2000) // Replace with a more robust synchronization mechanism if needed

        // Verify tip amount
        val expectedTip: String = NumberFormat.getCurrencyInstance().format(2.0)
        onView(withText("Tip Amount: $expectedTip"))
            .check(matches(withText("No node with this text was found.")))
    }
}