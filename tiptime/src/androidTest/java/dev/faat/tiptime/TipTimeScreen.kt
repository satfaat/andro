package dev.faat.tiptime

import androidx.compose.ui.test.junit4.ComposeTestRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performTextInput


class TipTimeScreen(private val composeTestRule: ComposeTestRule) {

    // Elements
    private val billAmountTextField = composeTestRule.onNodeWithText("Bill Amount")
    private val tipPercentageTextField = composeTestRule.onNodeWithText("Tip Percentage")
    private fun tipAmountText(amount: String) = composeTestRule.onNodeWithText("Tip Amount: $amount")

    // Actions
    fun enterBillAmount(amount: String): TipTimeScreen {
        billAmountTextField.performTextInput(amount)
        return this
    }

    fun enterTipPercentage(percentage: String): TipTimeScreen {
        tipPercentageTextField.performTextInput(percentage)
        return this
    }

    fun assertTipAmountDisplayed(expectedTip: String): TipTimeScreen {
        tipAmountText(expectedTip).assertExists("Tip amount not displayed")
        return this
    }
}