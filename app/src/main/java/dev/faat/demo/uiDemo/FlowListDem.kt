package dev.faat.demo.uiDemo

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.faat.demo.ui.theme.DemoTheme

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ScaffoldDemo() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Compose - Flow List") }
            )
        },
        content = { FlowListScreen() }
    )
}


@Composable
fun FlowListScreen() {
    val colorsFlow = flow<List<String>> {
        delay(1000)
        val list = listOf("Red", "Green", "Yellow", "Blue", "Black")
        emit(list)
    }

    val colors by colorsFlow
        .collectAsState(initial = emptyList())

    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        LazyColumn(
            Modifier
                .fillMaxSize()
                .padding(12.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            items(colors) { color ->
                Card(modifier = Modifier.fillMaxWidth()) {
                    Text(
                        text = color,
                        modifier = Modifier.padding(24.dp)
                    )
                }
            }
        }
    }
}


@Composable
@Preview(name = "Light Mode", apiLevel = 33)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode",
    apiLevel = 33
)
fun FlowListScreenPreview() {
    DemoTheme {
        ScaffoldDemo()
    }
}