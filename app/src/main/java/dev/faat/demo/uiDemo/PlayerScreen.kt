package dev.faat.demo.uiDemo

import android.content.res.Configuration
import android.media.MediaPlayer
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.faat.demo.ui.theme.DemoTheme
import dev.faat.demo.ui.theme.greenColor

@Composable
fun PlayerScreen() {
    val ctx = LocalContext.current
    val mediaPlayer = MediaPlayer()

    Surface(color = Color.Black) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .fillMaxSize()
                .padding(6.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {

            Text(
                modifier = Modifier
                    .padding(6.dp)
                    .width(90.dp),
                text = "Radio station",
                fontWeight = FontWeight.Bold,
                color = greenColor,
                fontSize = 20.sp
            )
            //        Spacer(modifier = Modifier.height(20.dp))
            // play button
            Button(
                modifier = Modifier
                    .width(100.dp)
                    .padding(1.dp),
                onClick = {
                    // on below line we are creating a variable for our audio url
                    // https://stream.zeno.fm/ugzz955srnhvv
                    // https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3

                    val audioUrl = "https://stream.zeno.fm/ugzz955srnhvv"

                    // on below line we are setting audio stream type as
                    // stream music on below line.
//                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)


                    // on below line we are running a try and catch block
                    // for our media player.
                    try {
                        // on below line we are setting audio source
                        // as audio url on below line.
                        mediaPlayer.setDataSource(audioUrl)

                        // on below line we are preparing
                        // our media player.
                        mediaPlayer.prepare()

                        // on below line we are starting
                        // our media player.
                        mediaPlayer.start()

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    // on below line we are displaying a toast message as audio player.
                    Toast.makeText(ctx, "Audio started playing..", Toast.LENGTH_SHORT).show()

                }) {
                // btn name
                Text(text = "Play")
            }
            //            Spacer(modifier = Modifier.height(20.dp))
            // on below line we are
            // creating a button for displaying a toast
            // stop button
            Button(
                modifier = Modifier
                    .width(100.dp)
                    .padding(1.dp),
                onClick = {
                    // on below line we are checking
                    // if media player is playing.
                    if (mediaPlayer.isPlaying) {
                        // if media player is playing
                        // we are stopping it on below line.
                        mediaPlayer.stop()

                        // on below line we are resetting
                        // our media player.
//                        mediaPlayer.reset()

                        // on below line we are calling release
                        // to release our media player.
//                        mediaPlayer.release()

                        // on below line we are displaying a toast message to pause audio
                        Toast.makeText(ctx, "Audio has been  stoped..", Toast.LENGTH_SHORT).show()
                    } else {
                        // if audio player is not displaying we are displaying
                        // below toast message
                        Toast.makeText(ctx, "Audio not played..", Toast.LENGTH_SHORT).show()
                    }
                }) {
                Text(text = "Stop")
            }
        }
    }
}

@Preview(name = "Light Mode", apiLevel = 33)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun PreviewPlayerScreen() {
    DemoTheme {
        PlayerScreen()
    }
}