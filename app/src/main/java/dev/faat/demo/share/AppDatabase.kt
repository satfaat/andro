package dev.faat.demo.share

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dev.faat.demo.configs.DB_NAME
import dev.faat.demo.todo1.data.Todo
import dev.faat.demo.todo1.data.TodoDao
import dev.faat.demo.user.data.room.User
import dev.faat.demo.user.data.room.UserDao

import dev.faat.demo.word.data.Word
import dev.faat.demo.word.data.WordDao


@Database(entities = [Todo::class, User::class, Word::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun todoDao(): TodoDao
    abstract fun userDao(): UserDao
    abstract fun wordDao(): WordDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DB_NAME
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}