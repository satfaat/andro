package dev.faat.demo.share.data.tmp

import dev.faat.demo.R

class CityDataSource {
    fun loadCities(): List<City> {
        return listOf(
            City(1, R.string.spain, R.drawable.business),
            City(2, R.string.new_york, R.drawable.architecture),
            City(3, R.string.tokyo, R.drawable.crafts),
            City(4, R.string.switzerland, R.drawable.design),
            City(6, R.string.singapore, R.drawable.drawing),
            City(7, R.string.paris, R.drawable.culinary),
        )
    }
}