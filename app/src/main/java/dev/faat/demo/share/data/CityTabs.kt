package dev.faat.demo.share.data

class CityTabs {
    fun initTabs(): MutableList<String> {
        return arrayListOf(
            "Spain",
            "New York",
            "Tokyo",
            "Switzerland",
            "Singapore",
            "Paris"
        )
    }
}