package dev.faat.demo.woof.data

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import dev.faat.demo.R

data class Dog(
    @DrawableRes val imageResourceId: Int,
    @StringRes val name: Int,
    val age: Int,
    @StringRes val hobbies: Int
)

//val dogs = listOf(
//    Dog(R.drawable.koda, R.string.dog_name_1, 2, R.string.dog_description_1),
//    Dog(R.drawable.lola, R.string.dog_name_2, 16, R.string.dog_description_2)
//)
