package dev.faat.demo.login.ui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import dev.faat.demo.R
import dev.faat.demo.login.data.AuthenticationState
import dev.faat.demo.login.data.TestTags

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PasswordInputField(
    authState: AuthenticationState,
    onValueChanged: (String) -> Unit,
    passwordToggleVisibility: (Boolean) -> Unit,
    text: String
) {
    OutlinedTextField(
        value = authState.password,
        onValueChange = { onValueChanged(it) },
        label = { Text(text = text) },
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
            .testTag(TestTags.LoginContent.PASSWORD_FIELD),
        enabled = !authState.loading,
        trailingIcon = {
            IconButton(onClick =
            {
                if (!authState.loading) passwordToggleVisibility(!authState.togglePasswordVisibility)
            })
            {
                Icon(
                    imageVector = if (authState.togglePasswordVisibility) Icons
                        .Default.Visibility
                    else Icons.Default.VisibilityOff,
                    contentDescription = stringResource(id = R.string.toggle)
                )
            }
        },
        visualTransformation = if (authState.togglePasswordVisibility) VisualTransformation
            .None else PasswordVisualTransformation(),
        colors = OutlinedTextFieldDefaults.colors(
            focusedBorderColor = Color.Blue,
            unfocusedBorderColor = Color.Black
        ),
        maxLines = 1,
        singleLine = true
    )
}