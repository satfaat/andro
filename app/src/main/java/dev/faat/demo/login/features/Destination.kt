package dev.faat.demo.login.features

sealed class Destination(val route: String) {
    object LoginScreen : Destination("login_screen")
    object RegisterScreen : Destination("register_screen")
}