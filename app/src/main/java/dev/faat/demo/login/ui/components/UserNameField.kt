package dev.faat.demo.login.ui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.faat.demo.R
import dev.faat.demo.login.data.AuthenticationState
import dev.faat.demo.login.data.TestTags

@Composable
fun UserNameField(
    authState: AuthenticationState,
    onValueChanged: (String) -> Unit,
    isError: Boolean = authState.userName.contains("[^a-zA-z0-9]".toRegex())
) {
    OutlinedTextField(
        value = authState.userName,
        onValueChange = {
            onValueChanged(it)
        },
        enabled = !authState.loading,
        label = { Text(stringResource(id = R.string.enter_name)) },
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
            .testTag(TestTags.LoginContent.USERNAME_FIELD),
        colors = OutlinedTextFieldDefaults.colors(
            focusedBorderColor = Color.Blue,
            unfocusedBorderColor = Color.Black
        ),
        isError = isError,
        maxLines = 1,
        singleLine = true
    )

    if (authState.userName.contains("[^a-zA-z0-9]".toRegex())) {
        Text(
            text = stringResource(id = R.string.error),
            fontSize = 14.sp,
            modifier = Modifier.padding(horizontal = 30.dp, vertical = 6.dp)
        )
    }
}


@Preview(showBackground = true)
@Composable
private fun UserNameFieldPreview() {
//    UserNameField()
}
