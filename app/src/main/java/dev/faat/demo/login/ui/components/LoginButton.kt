package dev.faat.demo.login.ui.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.faat.demo.login.data.TestTags
import dev.faat.demo.ui.components.CityApp

@Composable
fun LoginButton(
    isLoading: Boolean,
    enabled: Boolean = false,
    onLoginClicked: () -> Unit,
    text: String
) {

    Button(
        onClick = onLoginClicked,
        enabled = enabled,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 24.dp)
            .testTag(TestTags.LoginContent.SIGN_IN_BUTTON),
        shape = RoundedCornerShape(20.dp),
        border = BorderStroke(2.dp, Color.Gray),
        colors = ButtonDefaults.buttonColors(
            contentColor = Color.Blue,
            containerColor = Color.White
        )
    ) {
        if (isLoading) {
            AnimatedVisibility(visible = isLoading) {
                Row {
                    CircularProgressIndicator(
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        } else {
            Text(
                text = text,
                fontSize = 14.sp,
                modifier = Modifier.padding(horizontal = 30.dp, vertical = 6.dp)
            )
        }

    }
}


@Preview(showBackground = true)
@Composable
private fun LoginButtonPreview() {
//    LoginButton()
}