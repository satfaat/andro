package dev.faat.demo.login.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import dev.faat.demo.login.features.Destination
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController


@Composable
fun Navigation(navController: NavHostController) {
    NavHost(navController, startDestination = Destination.LoginScreen.route) {
        composable(Destination.LoginScreen.route) {
            LoginContentScreen(loginViewModel = hiltViewModel(), onRegisterNavigateTo = {
                navController.navigate(Destination.RegisterScreen.route)
            })
        }
        composable(Destination.RegisterScreen.route) {
            RegisterContentScreen(registerViewModel = hiltViewModel())
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun NavigationPreview() {
//    Navigation(rememberNavController())
}