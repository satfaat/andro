package dev.faat.demo.login.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.tooling.preview.Preview
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.faat.demo.login.viewModel.LoginViewModel

@Composable
fun LoginContentScreen(
    loginViewModel: LoginViewModel,
    onRegisterNavigateTo: () -> Unit
) {
    val viewState by loginViewModel.state.collectAsState()

    LoginContent(
        uiState = viewState,
        onUsernameUpdated = loginViewModel::userNameChanged,
        onPasswordUpdated = loginViewModel::passwordChanged,
        onLogin = loginViewModel::login,
        passwordToggleVisibility = loginViewModel::passwordVisibility,
        onRegister = onRegisterNavigateTo
    )
}


@Preview(showBackground = true)
@Composable
private fun LoginScreenPreview() {
//    LoginContentScreen(loginViewModel = LoginViewModel(),
//        onRegisterNavigateTo = {})
}