package dev.faat.demo.affirmations.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import dev.faat.demo.affirmations.data.Datasource
import dev.faat.demo.affirmations.ui.components.AffirmationList

class AffirmationScreen {
}


@Composable
fun AffirmationsApp() {
    AffirmationList(
        affirmationList = Datasource().loadAffirmations(),
    )
}


@Preview
@Composable
private fun AffirmationCardPreview() {
//    AffirmationCard(Affirmation(R.string.affirmation1, R.drawable.image1))
    AffirmationsApp()
}