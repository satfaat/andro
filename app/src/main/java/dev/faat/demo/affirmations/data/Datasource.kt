package dev.faat.demo.affirmations.data

import dev.faat.demo.R
import dev.faat.demo.affirmations.data.model.Affirmation

class Datasource {
    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(R.string.affirmation1, R.drawable.image1),
            Affirmation(R.string.affirmation2, R.drawable.image2),
        )
    }
}