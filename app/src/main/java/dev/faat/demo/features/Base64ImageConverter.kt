package feature

import java.io.ByteArrayInputStream
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64


object Base64ImageConverter {

    /**
     * Converts a Base64 encoded string to a Bitmap image.
     *
     * @param base64String The Base64 encoded string representing the image.
     * @return A Bitmap image, or null if the conversion fails (e.g., invalid Base64 string).
     */
    fun base64ToBitmap(base64String: String?): Bitmap? {
        if (base64String.isNullOrBlank()) {
            return null // Handle null or empty input
        }

        try {
            val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
            val inputStream = ByteArrayInputStream(decodedBytes)
            return BitmapFactory.decodeStream(inputStream)
        } catch (e: IllegalArgumentException) {
            // Handle invalid Base64 string
            println("Error decoding Base64 string: ${e.message}")
            return null
        } catch (e: Exception) {
            // Handle other potential exceptions (e.g., out of memory)
            println("Error converting Base64 to Bitmap: ${e.message}")
            return null
        }
    }

    /**
     * Converts a Base64 encoded string to a ByteArray. This is useful if you need
     * the raw image data instead of a Bitmap.
     *
     * @param base64String The Base64 encoded string.
     * @return A ByteArray containing the decoded image data, or null on failure.
     */
    fun base64ToByteArray(base64String: String?): ByteArray? {
        if (base64String.isNullOrBlank()) {
            return null
        }

        try {
            return Base64.decode(base64String, Base64.DEFAULT)
        } catch (e: IllegalArgumentException) {
            println("Error decoding Base64 string: ${e.message}")
            return null
        }
    }
}