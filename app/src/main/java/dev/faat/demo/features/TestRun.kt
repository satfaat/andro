package dev.faat.demo.features

import android.widget.ImageView
import dev.faat.demo.R
import feature.Base64ImageConverter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


suspend fun main() {
    val base64ImageString =
        ""


    val bitmap = withContext(Dispatchers.IO) { // Perform decoding on IO thread
        Base64ImageConverter.base64ToBitmap(base64ImageString)
    }

    if (bitmap != null) {
        println(bitmap)
    }

//    println(Base64ImageConverter.base64ToBitmap(base64ImageString))
}


