package dev.faat.demo.todo1.data

import android.content.Context
import dev.faat.demo.share.AppDatabase

/**
 * App container for Dependency injection.
 */
interface AppContainer {
    val todoRepository: TodoRepository
}

/**
 * [AppContainer] implementation that provides instance of [OfflineTodoRepository]
 */
class AppDataContainer(private val context: Context) : AppContainer {
    /**
     * Implementation for [TodoRepository]
     */
    override val todoRepository: TodoRepository by lazy {
        OfflineTodoRepository(AppDatabase.getDatabase(context).todoDao())
    }
}