package dev.faat.demo.todo1.ui.todo

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
//noinspection UsingMaterialAndMaterial3Libraries
import androidx.compose.material.OutlinedTextField
//noinspection UsingMaterialAndMaterial3Libraries
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import dev.faat.demo.R
import dev.faat.demo.todo1.data.Todo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


private val dao = TodoApplication.dbConn.todoDao()
var todoList = mutableStateListOf<Todo>()
private var scope = MainScope()

private fun loadToDo() {
    scope.launch {
        withContext(Dispatchers.Default) {
            dao.getAllTasks()
        }
    }
}


private fun postTodo(todo: Todo) {
    scope.launch {
        withContext(Dispatchers.Default) {
            dao.insert(todo)
            todoList.clear()
            loadToDo()
        }
    }
}

private fun deleteTodo(todo: Todo) {
    scope.launch {
        withContext(Dispatchers.Default) {
            dao.delete(todo)
            todoList.clear()
            loadToDo()
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun TodoScreen(todoList: SnapshotStateList<Todo>) {
    val context = LocalContext.current
    val keyboardController: SoftwareKeyboardController? = LocalSoftwareKeyboardController.current
    var text: String by remember {
        mutableStateOf("")
    }

    Column(
        modifier = Modifier.clickable {
            keyboardController?.hide()
        }
    ) {
        TopAppBar(
            title = { Text(text = stringResource(id = R.string.todo_title)) }
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            OutlinedTextField(
                value = text,
                onValueChange = { text = it },
                colors = TextFieldDefaults.textFieldColors(
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent
                ),
                modifier = Modifier.border(
                    BorderStroke(2.dp, Color.Gray)
                ),
                label = { Text(text = stringResource(id = R.string.new_todo)) }
            )

            Spacer(modifier = Modifier.size(16.dp))

//            Button(
//                onClick = {
//                    if (text.isEmpty()) return@Button
//                    postTodo(text)
//                    text = ""
//                },
//                modifier = Modifier.align(Alignment.CenterVertically)
//            ) {
//                Text(text = stringResource(id = R.string.add_todo))
//            }
        }
    }
}