package dev.faat.demo.todo1.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "todo")
data class Todo(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String,
    val description: String,
//    @ColumnInfo(name = "created_at") val createdAt: Date = Date(),
    @ColumnInfo(name = "is_finished") val isFinished: Boolean = false
)