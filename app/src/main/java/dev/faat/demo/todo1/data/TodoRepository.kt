package dev.faat.demo.todo1.data

import dev.faat.demo.todo1.data.Todo
import kotlinx.coroutines.flow.Flow


//class TodoRepository(private val todoDao: TodoDao) {
//
//    val allTodo: Flow<List<Todo>> = todoDao.getAllTasks()
//
//    suspend fun insert(todo: Todo) {
//        todoDao.insert(todo)
//    }
//
//    suspend fun delete(todo: Todo) {
//        todoDao.delete(todo)
//    }
//
//    suspend fun update(todo: Todo) {
//        todoDao.update(todo)
//    }
//}

interface TodoRepository {

    fun getAllTasksStream(): Flow<List<Todo>>

    suspend fun insert(todo: Todo)

    suspend fun delete(todo: Todo)

    suspend fun update(todo: Todo)
}