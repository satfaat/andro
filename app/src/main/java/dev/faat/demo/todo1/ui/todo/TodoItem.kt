package dev.faat.demo.todo1.ui.todo

import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.faat.demo.todo1.data.Todo
import dev.faat.demo.ui.theme.DemoTheme

@Composable
fun TodoItem(
    todo: Todo,
    onClick: (todo: Todo) -> Unit = {}
) {
//    val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH)

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clickable {
                onClick(todo)
            }
    ) {
        Text(
            text = todo.title,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 4.dp)
        )

//        Text(
//            text = "${stringResource(id = R.string.todo_created_at)} ${sdf.format(todo.createdAt)}",
//            fontSize = 12.sp,
//            color = Color.LightGray,
//            textAlign = TextAlign.Right,
//            modifier = Modifier.fillMaxWidth()
//        )
    }
}


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun PreviewMessageCard() {
    DemoTheme {
        val todo1 = Todo(
            1,
            "Some Todo",
            "Desc ...",
            false
        )
        TodoItem(todo1)
    }
}