package dev.faat.demo.todo1.data

import kotlinx.coroutines.flow.Flow

class OfflineTodoRepository(private val todoDao: TodoDao) : TodoRepository {
    override fun getAllTasksStream(): Flow<List<Todo>> = todoDao.getAllTasks()

    override suspend fun insert(todo: Todo) = todoDao.insert(todo)

    override suspend fun delete(todo: Todo) = todoDao.delete(todo)

    override suspend fun update(todo: Todo) = todoDao.update(todo)
}