package dev.faat.demo.todo1.ui.todo

import android.app.Application
import dev.faat.demo.share.AppDatabase


class TodoApplication : Application() {

    companion object {
        lateinit var dbConn: AppDatabase
    }

    override fun onCreate() {
        super.onCreate()
        dbConn = AppDatabase.getDatabase(applicationContext)
    }
}