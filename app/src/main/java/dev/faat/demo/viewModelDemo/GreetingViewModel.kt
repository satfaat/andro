package dev.faat.demo.viewModelDemo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import dev.faat.demo.ui.theme.DemoTheme
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow


class GreetingActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MaterialTheme {
                Column {
                    GreetingScreen("user1")
                    GreetingScreen("user2")
                }
            }
        }
    }
}


@Composable
fun GreetingScreen(
    userId: String,
    viewModel: GreetingViewModel = viewModel(
        factory = GreetingViewModelFactory(userId)
    )
) {
    //val messageUser by viewModel.message.observeAsState("")
    // Use collectAsState to convert a StateFlow to a State
    val messageUser by viewModel.message.collectAsState("")
    Text(messageUser)
}

class GreetingViewModel(private val userId: String) : ViewModel() {
    // For data state. Declare a MutableStateFlow variable
    // Init value.
    // private val _message: MutableLiveData<String> = MutableLiveData("Hi $userId")
    // val message: LiveData<String> = _message
    private val _message: MutableStateFlow<String> = MutableStateFlow("Hi, $userId")
    val message: StateFlow<String> = _message
}

class GreetingViewModelFactory(private val userId: String) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return GreetingViewModel(userId) as T
    }
}


@Preview(showBackground = true, apiLevel = 33)
@Composable
private fun LoginScreenPreview() {
    DemoTheme {
        GreetingScreen("Faat")
    }
}
