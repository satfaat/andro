package dev.faat.demo.syntax.oop

// Use scope functions to access class properties and methods

class Quiz3 {
    val question1 = QuestionWithEnum<String>("Quoth the raven ___", "nevermore", Difficulty.MEDIUM)
    val question2 =
        QuestionWithEnum<Boolean>("The sky is green. True or false", false, Difficulty.EASY)
    val question3 =
        QuestionWithEnum<Int>("How many days are there between full moons?", 28, Difficulty.HARD)

    companion object StudentProgress3 {
        var total: Int = 10
        var answered: Int = 3
    }

    fun printQuiz1() {
        println(question1.questionText)
        println(question1.answer)
        println(question1.difficulty)
        println()
        println(question2.questionText)
        println(question2.answer)
        println(question2.difficulty)
        println()
        println(question3.questionText)
        println(question3.answer)
        println(question3.difficulty)
        println()
    }

    // Replace long object names using let()
    fun printQuiz2() {
        question1.let {
            println(it.questionText)
            println(it.answer)
            println(it.difficulty)
        }
        println()
        question2.let {
            println(it.questionText)
            println(it.answer)
            println(it.difficulty)
        }
        println()
        question3.let {
            println(it.questionText)
            println(it.answer)
            println(it.difficulty)
        }
        println()
    }

}

fun main() {
    if (false) {
        val quiz = Quiz3()
        quiz.printQuiz2()
    }

    if (false) {
        // Call an object's methods without a variable using apply()
        val quiz = Quiz3().apply {
            printQuiz2()
        }
    }

    if (true) {
        // Call an object's methods without a variable using apply()
        Quiz3().apply {
            printQuiz2()
        }
    }
}