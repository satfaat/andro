package dev.faat.demo.syntax.oop


internal class PersonJava {
    val name: String
    val age: Int?

    constructor(name: String) {
        this.name = name
        age = null
    }

    constructor(name: String, age: Int?) {
        this.name = name
        this.age = age
    }
}