package dev.faat.demo.syntax.oop

// Add an extension property
val Quiz.StudentProgress.progressText: String
    get() = "${answered} of ${total} answered"

// Add an extension function
fun Quiz.StudentProgress.printProgressBar() {
    repeat(Quiz.answered) { print("▓") }
    repeat(Quiz.total - Quiz.answered) { print("▒") }
    println()
    println(Quiz.progressText)
}


fun main() {
//    println(Quiz.progressText)
    Quiz.printProgressBar()
}