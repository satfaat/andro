package dev.faat.demo.syntax.oop


// it makes any class that extends it able to print a progress bar
interface ProgressPrintable {
    val progressText2: String
    fun printProgressBar2()
}

class Quiz2 : ProgressPrintable {
    val question1 = QuestionWithEnum<String>("Quoth the raven ___", "nevermore", Difficulty.MEDIUM)
    val question2 =
        QuestionWithEnum<Boolean>("The sky is green. True or false", false, Difficulty.EASY)
    val question3 =
        QuestionWithEnum<Int>("How many days are there between full moons?", 28, Difficulty.HARD)

    companion object StudentProgress2 {
        var total: Int = 10
        var answered: Int = 3
    }

    override val progressText2: String
        get() = "${answered} of ${total} answered"

    override fun printProgressBar2() {
        repeat(Quiz.answered) { print("▓") }
        repeat(Quiz.total - Quiz.answered) { print("▒") }
        println()
        println(progressText2)
    }

}


fun main() {
    Quiz2().printProgressBar2()
}