package dev.faat.demo.syntax.oop

/*
* Make a reusable class with generics
* */
// before generic data type
class FillInTheBlankQuestion(
    val questionText: String,
    val answer: String,
    val difficulty: String
)

class TrueOrFalseQuestion(
    val questionText: String,
    val answer: Boolean,
    val difficulty: String
)

class NumericQuestion(
    val questionText: String,
    val answer: Int,
    val difficulty: String
)

//Refactor your code to use generics
class Question<T>(
    val questionText: String,
    val answer: T,
    val difficulty: String
)

// Use an enum constant
// enum classes define a limited set of possible values
enum class Difficulty {
    EASY, MEDIUM, HARD
}

// data class
// Convert Question to a data class
data class QuestionWithEnum<T>(
    val questionText: String,
    val answer: T,
    val difficulty: Difficulty
)


fun main() {

    // without Enum
    val question1 = Question<String>("Quoth the raven ___", "nevermore", "medium")
    val question2 = Question<Boolean>("The sky is green. True or false", false, "easy")
    val question3 = Question<Int>("How many days are there between full moons?", 28, "hard")

    // with enum, easy to mantain
    val questionEnum =
        QuestionWithEnum<String>("Quoth the raven ___", "nevermore", Difficulty.MEDIUM)
    val questionEnum2 =
        QuestionWithEnum<Boolean>("The sky is green. True or false", false, Difficulty.EASY)
    val questionEnum3 =
        QuestionWithEnum<Int>("How many days are there between full moons?", 28, Difficulty.HARD)
    println(questionEnum.toString())
}