package dev.faat.demo.syntax.oop

// singleton object

object StudentProgressDem {
    var total: Int = 10
    var answered: Int = 3
}

// Declare objects as companion objects

class Quiz {
    val question1 = QuestionWithEnum<String>("Quoth the raven ___", "nevermore", Difficulty.MEDIUM)
    val question2 =
        QuestionWithEnum<Boolean>("The sky is green. True or false", false, Difficulty.EASY)
    val question3 =
        QuestionWithEnum<Int>("How many days are there between full moons?", 28, Difficulty.HARD)

//    is restricted to one instance
    companion object StudentProgress {
        var total: Int = 10
        var answered: Int = 3
    }
}


fun main() {
    println("${StudentProgressDem.answered} of ${StudentProgressDem.total} answered.")
    println("${Quiz.answered} of ${Quiz.total} answered.")
}