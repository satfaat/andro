package dev.faat.demo.syntax.oop

//open class Outer(word: String) {
//
//    // Nested class for general locators
//    inner class Inner(val word: String) {
//        fun getWord(): String {
//            return word
//        }
//    }
//
//    val inner = Inner("Soul")
//    fun print() = println(inner.getWord())
//}
//
//fun main() {
//    val obj = Outer("Dark")
//    obj.print()
//}