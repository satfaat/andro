package dev.faat.demo.syntax.oop


enum class Daypart {
    MORNING,
    AFTERNOON,
    EVENING,
}

data class Event(
    val title: String,
    val description: String? = null,  //can be null
    val daypart: Daypart,
    val durationInMinutes: Int,
)