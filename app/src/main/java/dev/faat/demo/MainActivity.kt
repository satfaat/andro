package dev.faat.demo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import dev.faat.demo.ui.theme.DemoTheme

//@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DemoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
//                    MessageCard(Message("Andro", "Jetpack"))
//                    InventoryApp()
//                    PlayerScreen()
//                    TempConverterScreen()

//                    Navigation(navController = rememberNavController())

//                    val viewModel = ViewModelProvider(this)[UserViewModel::class.java]
//
//                    // Compose the UserScreen
//                    UserScreen(viewModel)
                }
            }
        }
    }
}




