package dev.faat.demo.roomDemo.data


data class Message(val author: String, val body: String)
