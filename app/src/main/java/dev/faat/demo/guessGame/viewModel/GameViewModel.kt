package dev.faat.demo.guessGame.viewModel


import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class GameViewModel : ViewModel() {
    private val words = listOf("Android", "Activity", "Fragment")
    private val secretWord = words.random().uppercase()

    private val _secretWordDisplay = MutableStateFlow<String>("")
    val secretWordDisplay: StateFlow<String> = _secretWordDisplay

    private var correctGuesses = ""

    private val _incorrectGuesses = MutableStateFlow<String>("")
    val incorrectGuesses: StateFlow<String> = _incorrectGuesses

    private val _livesLeft = MutableStateFlow<Int>(8)
    val livesLeft: StateFlow<Int> = _livesLeft

    private val _gameOver = MutableStateFlow<Boolean>(false)
    val gameOver: StateFlow<Boolean> = _gameOver

    init {
        _secretWordDisplay.value = deriveSecretWordDisplay()
    }

    private fun deriveSecretWordDisplay(): String {
        var display = ""
        secretWord.forEach {
            display += checkLetter(it.toString())
        }
        return display
    }

    private fun checkLetter(str: String) = when (correctGuesses.contains(str)) {
        true -> str
        false -> "_"
    }

    fun makeGuess(guess: String) {
        if (guess.length == 1) {
            if (secretWord.contains(guess)) {
                correctGuesses += guess
                _secretWordDisplay.value = deriveSecretWordDisplay()
            } else {
                _incorrectGuesses.value += "$guess "
                _livesLeft.value = livesLeft.value.minus(1)
            }
        }
        if (isWon() || isLost()) _gameOver.value = true
    }

    private fun isWon() = secretWord.equals(secretWordDisplay.value, true)

    private fun isLost() = (livesLeft.value ?: 0) <= 0

    fun wonLostMessage(): String {
        var message = ""
        if (isWon()) message = "You won!"
        else if (isLost()) message = "You lost!"
        message += " The word was $secretWord."
        return message
    }

    fun finishGame() {
        _gameOver.value = true
    }
}