package dev.faat.demo.guessGame.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material3.Surface
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import dev.faat.demo.guessGame.viewModel.GameViewModel
import dev.faat.demo.ui.theme.DemoTheme

//class GameFragment : Fragment() {
//    lateinit var viewModel: GameViewModel
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        viewModel = ViewModelProvider(this)[GameViewModel::class.java]
//
//        viewModel.gameOver.collect{ newValue ->
//            if (newValue) {
//                val action = GameFragmentDirections
//                    .actionGameFragmentToResultFragment(viewModel.wonLostMessage())
//                view?.findNavController()?.navigate(action)
//            }
//        }
//        return ComposeView(requireContext()).apply {
//            setContent {
//                DemoTheme {
//                    Surface {
//                        GameFragmentContent(viewModel)
//                    }
//                }
//            }
//        }
//    }
//}