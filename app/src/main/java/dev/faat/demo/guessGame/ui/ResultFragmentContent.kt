package dev.faat.demo.guessGame.ui

import android.view.View
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import dev.faat.demo.guessGame.viewModel.ResultViewModel

@Composable
fun ResultFragmentContent(
    view: View,
    viewModel: ResultViewModel
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ResultText(viewModel.result)
//        Button1({
//            view.findNavController()
//                .navigate(R.id.action_resultFragment_to_gameFragment)
//        }, "Start New Game")
    }
}


@Composable
fun ResultText(result: String) {
    Text(
        text = result,
        fontSize = 28.sp,
        textAlign = TextAlign.Center
    )
}