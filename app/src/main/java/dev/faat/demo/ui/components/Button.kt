package dev.faat.demo.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import dev.faat.demo.R

@Composable
fun Button1(clicked: () -> Unit, title: String) {
    Button(onClick = clicked) {
        Text(title)
    }
}

@Composable
@Preview(showBackground = true)
//@PreviewParameter(ButtonTitleProvider::class)
fun Button1Preview() {
    Button1(clicked = {}, title = "Click Me")
}

@Composable
fun TextButton(clicked: () -> Unit, text: String) {
    Box(modifier = Modifier.clickable(onClick = clicked)) {
        Text(text = text)
    }
}

@Composable
@Preview(showBackground = true)
fun TextButtonPreview() {
    TextButton(clicked = {}, text = "Click Me")
}

@Composable
fun ImageButton(clicked: () -> Unit) {
    Box(modifier = Modifier.clickable(onClick = clicked)) {
        Icon(
            painterResource(id = R.drawable.baseline_add_24),
            contentDescription = ""
        )
    }
}

@Composable
@Preview(showBackground = true)
fun ImageButtonPreview() {
    ImageButton(clicked = {})
}

class ButtonTitleProvider : PreviewParameterProvider<String> {
    override val values = sequenceOf("Click Me", "Submit", "Learn More")
}