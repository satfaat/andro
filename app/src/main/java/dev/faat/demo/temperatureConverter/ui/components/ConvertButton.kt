package dev.faat.demo.temperatureConverter.ui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun ConvertButton(clicked: () -> Unit) {
    Button(
        onClick = clicked,
        modifier = Modifier
            .width(270.dp)
            .height(70.dp)
    ) {
        Text("Convert")
    }
}

@Preview(showBackground = true)
@Composable
private fun ConvertButtonPreview() {
    val celsius = remember { mutableStateOf(0) }
    val newCelsius = remember { mutableStateOf("") }
    ConvertButton {
        newCelsius.value.toIntOrNull()?.let {
            celsius.value = it
        }
    }
}