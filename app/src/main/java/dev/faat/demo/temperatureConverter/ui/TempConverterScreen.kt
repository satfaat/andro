package dev.faat.demo.temperatureConverter.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.faat.demo.R
import dev.faat.demo.temperatureConverter.ui.components.ConvertButton


@Composable
fun TempConverterScreen() {
    val celsius = remember { mutableIntStateOf(0) }
    val newCelsius = remember { mutableStateOf("") }
    Column(
        modifier = Modifier
            .padding(1.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
//        verticalArrangement = Arrangement.Center
    ) {
        Header(R.drawable.image1, "sunrise image")

        Spacer(Modifier.height(260.dp))
        EnterTemperature(newCelsius.value) {
            newCelsius.value = it
        }
        ConvertButton {
            newCelsius.value.toIntOrNull()?.let {
                celsius.intValue = it
            }
        }
        TemperatureText(celsius.intValue)
    }
}


@Composable
fun TemperatureText(celsius: Int) {
    val fahrenheit = (celsius.toDouble() * 9 / 5) + 32
    Text("$celsius Celsius is $fahrenheit Fahrenheit")
}

@Composable
fun Header(image: Int, description: String) {
    Image(
        painter = painterResource(image),
        contentDescription = description,
        modifier = Modifier
            .height(180.dp)
            .fillMaxWidth(),
        contentScale = ContentScale.Crop
    )
}


@Preview(showBackground = true)
@Composable
fun PreviewTempConverterScreen() {
    TempConverterScreen()
}