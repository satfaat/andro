package dev.faat.demo.user.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    // Find a user by their id
    @Query("SELECT * FROM users WHERE id = :id")
    fun findById(id: Int): Flow<User?>

    // Save a user to the database
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(user: User): Long

    @Update
    suspend fun update(user: User)

    // Delete a user from the database
    @Query("DELETE FROM users WHERE id = :userId")
    suspend fun delete(userId: Int)

    //@Query("SELECT * FROM users WHERE id IN (:id)")
    //fun getUsersByIds(id: IntArray)

//    @Delete
//    suspend fun delete(user: User)

    // Find all users in the database
    @Query("SELECT * FROM users")
    fun findAll(): Flow<List<User>>

    // Find users by their first name
    @Query("SELECT * FROM users WHERE first_name LIKE :firstName")
    fun findByFirstName(firstName: String): Flow<List<User>>

    // Find users by their last name
    @Query("SELECT * FROM users WHERE last_name LIKE :lastName")
    fun findByLastName(lastName: String): Flow<List<User>>

    // Find users by their email
    @Query("SELECT * FROM users WHERE email = :email")
    fun findByEmail(email: String): Flow<User?>


//    @Query(
//        "SELECT * FROM users WHERE first_name LIKE :first AND " +
//                "last_name LIKE :last LIMIT 1"
//    )
//    fun findByName(first: String, last: String): Flow<User>
}