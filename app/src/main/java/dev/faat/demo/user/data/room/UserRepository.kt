package dev.faat.demo.user.data.room

import kotlinx.coroutines.flow.Flow

class UserRepository(private val userDao: UserDao) {

    suspend fun getUserById(userId: Int): Flow<User?> {
        return userDao.findById(userId)
    }

    suspend fun saveUser(user: User): Long {
        return userDao.save(user)
    }

    suspend fun updateUser(user: User) {
        userDao.update(user)
    }

    suspend fun deleteUser(userId: Int) {
        userDao.delete(userId)
    }

    val getAllUsers: Flow<List<User>> = userDao.findAll()

    fun getUsersByFirstName(firstName: String): Flow<List<User>> {
        return userDao.findByFirstName(firstName)
    }

    fun getUsersByLastName(lastName: String): Flow<List<User>> {
        return userDao.findByLastName(lastName)
    }

    fun getUserByEmail(email: String): Flow<User?> {
        return userDao.findByEmail(email)
    }
}