package dev.faat.demo.user.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import dev.faat.demo.user.data.room.User
import dev.faat.demo.user.state.UserViewModel

@Composable
fun UserScreen(viewModel: UserViewModel) {
    val user = viewModel.user.collectAsState(initial = null)
    val users = viewModel.users.collectAsState(initial = emptyList())
    val error = viewModel.error.collectAsState(initial = null)

    // Show loading indicator while user is not yet loaded
    if (user.value == null) {
        CircularProgressIndicator()
    } else {
        // Display user details
        Column(modifier = Modifier.padding(16.dp)) {
            Text(text = "Name: ${user.value?.firstName ?: "N/A"}")
            Text(text = "Email: ${user.value?.email ?: "N/A"}")
            Button(onClick = {
                // Edit user
            }) {
                Text(text = "Edit")
            }
            Button(onClick = {
                // Delete user
            }) {
                Text(text = "Delete")
            }
        }
    }

    // Show error message if any
    if (error.value != null) {
        Text(text = "Error: ${error.value}", color = Color.Red)
    }

    // Show list of users if viewing all users
    if (users.value.isNotEmpty()) {
        Text(text = "Users:")
        UserList(users = users.value)
    }
}

@Composable
fun UserList(users: List<User>) {
    Column {
        users.forEach { user ->
            UserItem(user = user)
        }
    }
}

@Composable
fun UserItem(user: User) {
    Row(modifier = Modifier.padding(8.dp)) {
        Text(text = user.firstName)
        Text(text = user.email)
    }
}


//@Preview
//@Composable
//fun UserScreenPreviewLoading() {
//    val fakeViewModel = UserViewModel(FakeUserRepository()) // Use a fake repository for preview
//    UserScreen(viewModel = fakeViewModel)
//}
//
//@Preview
//@Composable
//fun UserScreenPreviewUser() {
//    val fakeViewModel = UserViewModel(FakeUserRepository().apply { setUser(User("John Doe", "johndoe@example.com")) })
//    UserScreen(viewModel = fakeViewModel)
//}
//
//@Preview
//@Composable
//fun UserScreenPreviewUsers() {
//    val fakeViewModel = UserViewModel(FakeUserRepository().apply { setUsers(listOf(User("Alice", "alice@example.com"), User("Bob", "bob@example.com"))) })
//    UserScreen(viewModel = fakeViewModel)
//}
//
//@Preview
//@Composable
//fun UserScreenPreviewError() {
//    val fakeViewModel = UserViewModel(FakeUserRepository().apply { setError("An error occurred") })
//    UserScreen(viewModel = fakeViewModel)
//}