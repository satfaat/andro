package dev.faat.demo.user.state

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import dev.faat.demo.user.data.room.User
import dev.faat.demo.user.data.room.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch

class UserViewModel(private val repository: UserRepository) : ViewModel() {

    private val _user = MutableStateFlow<User?>(null)
    val user: StateFlow<User?> = _user.asStateFlow()

    private val _users = MutableStateFlow<List<User>>(emptyList())
    val users: StateFlow<List<User>> = _users.asStateFlow()

    private val _error = MutableStateFlow<String?>(null)
    val error: StateFlow<String?> = _error.asStateFlow()

    fun getUserById(userId: Int) {
        viewModelScope.launch {
            try {
                val user = repository
                    .getUserById(userId)
                    .firstOrNull() // Collect the first value
                _user.value = user
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun saveUser(user: User) {
        viewModelScope.launch {
            try {
                repository.saveUser(user)
                _user.value = user
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun updateUser(user: User) {
        viewModelScope.launch {
            try {
                repository.updateUser(user)
                _user.value = user
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun deleteUser(userId: Int) {
        viewModelScope.launch {
            try {
                repository.deleteUser(userId)
                _user.value = null
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun getAllUsers() {
        viewModelScope.launch {
            try {
                repository.getAllUsers
                    .collect { users ->
                        _users.value = users
                    }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun getUsersByFirstName(firstName: String) {
        viewModelScope.launch {

            try {
                repository.getUsersByFirstName(firstName)
                    .collect { users ->
                        _users.value = users
                    }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun getUsersByLastName(lastName: String) {
        viewModelScope.launch {

            try {
                repository.getUsersByLastName(lastName)
                    .collect { users ->
                        _users.value = users
                    }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun getUserByEmail(email: String) {
        viewModelScope.launch {

            try {
                repository.getUserByEmail(email)
                    .collect { user ->
                        _user.value = user
                    }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun clearError() {
        _error.value = null
    }
}


class UserViewModelFactory(private val repository: UserRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}