package dev.faat.demo.user.state

import android.app.Application
import dev.faat.demo.share.AppDatabase
import dev.faat.demo.user.data.room.UserRepository

class UserApplication : Application() {

    // Use volatile for thread safety
    @Volatile
    private lateinit var database: AppDatabase

    val repository: UserRepository by lazy { UserRepository(database.userDao()) } // Inject UserDao into UserRepository

    override fun onCreate() {
        super.onCreate()
        database = AppDatabase.getDatabase(applicationContext)
    }
}