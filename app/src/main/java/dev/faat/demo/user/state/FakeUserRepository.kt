package dev.faat.demo.user.state

import dev.faat.demo.user.data.room.User
import dev.faat.demo.user.data.room.UserRepository
import kotlinx.coroutines.flow.Flow


//class FakeUserRepository(private val realRepository: UserRepository) : UserRepository {
//
//    private val users = mutableListOf<User>()
//
//    // Mock data for previews
//    init {
//        users.add(User(1, "John Doe", "johndoe@example.com"))
//        users.add(User(2, "Jane Doe", "janedoe@example.com"))
//    }
//
//    override suspend fun getUserById(userId: Int): Flow<User?> {
//        // Check if specific user error is set
//        setErrorForUserById(userId)?.let { throw Exception(it) }
//        return realRepository.getUserById(userId)
//    }
//
//    override suspend fun saveUser(user: User): Long {
//        // ... similar logic, potentially modifying user data or throwing mock errors
//        return realRepository.saveUser(user)
//    }
//
//    // ... implement other methods similarly
//
//    fun setErrorForUserById(userId: Int, error: String) {
//        // Store error for specific user
//    }
//
//    fun setErrorForGetAllUsers(error: String) {
//        // Store error for getAllUsers
//    }
//
//    // ... similar methods for other actions and error scenarios
//
//}

