package dev.faat.demo.word.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import dev.faat.demo.ui.theme.DemoTheme
import dev.faat.demo.word.state.WordViewModel2


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WordScreen(
    modifier: Modifier = Modifier,
    viewModel: WordViewModel2 = viewModel(factory = WordViewModel2.Factory)
) {
    val allWordsFlow = viewModel.allWords
//        .collectAsState(initial = emptyList())

    val allWords by allWordsFlow
        .collectAsState(initial = emptyList())

    Column(
        modifier = modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Row(
            modifier = modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextField(
                value = viewModel.newText.value,
                onValueChange = {
                    viewModel.newText.value = it
                },
                label = {
                    Text(text = "Name...")
                },
                modifier = Modifier.weight(1f),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.White
                )
            )
            IconButton(
                onClick = {
                    viewModel.insert()
                }) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Add"
                )
            }
        }
        Spacer(modifier = Modifier.height(5.dp))
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ) {
            items(allWords) { word ->
                WordList(
                    word, {
                        viewModel.word = it
                        viewModel.newText.value = it.word
                    },
                    {
                        viewModel.delete(it)
                    }
                )
            }
        }
    }
}

@Preview(showBackground = true, apiLevel = 33)
@Composable
fun WordScreenPreview() {
    DemoTheme {
        WordScreen()
    }
}