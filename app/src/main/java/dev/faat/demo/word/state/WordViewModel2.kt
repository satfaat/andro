package dev.faat.demo.word.state

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import dev.faat.demo.word.data.Word
import dev.faat.demo.word.data.WordRepository
import kotlinx.coroutines.launch

class WordViewModel2(private val repository: WordRepository) : ViewModel() {
    val allWords = repository.allWords
    val newText = mutableStateOf("")
    var word: Word? = null

    fun insert() = viewModelScope.launch {
        val nameItem = word?.copy(word = newText.value)
            ?: Word(id = 0, word = newText.value)
        repository.insert(nameItem)
        word = null
        newText.value = ""
    }

    fun delete(word: Word) = viewModelScope.launch {
        repository.delete(word)
    }

    // Define ViewModel factory in a companion object
    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
//                val savedStateHandle = createSavedStateHandle()
                val repository = (this[APPLICATION_KEY] as WordsApplication).repository
                WordViewModel2(
                    repository = repository
                )
            }
        }
    }
}
