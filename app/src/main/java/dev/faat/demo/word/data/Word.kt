package dev.faat.demo.word.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_words")
data class Word(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val word: String
)
