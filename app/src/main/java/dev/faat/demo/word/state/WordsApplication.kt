package dev.faat.demo.word.state

import android.app.Application
import dev.faat.demo.share.AppDatabase
import dev.faat.demo.word.data.WordRepository


class WordsApplication : Application() {
    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database by lazy { AppDatabase.getDatabase(this) }
    val repository by lazy { WordRepository(database.wordDao()) }
}