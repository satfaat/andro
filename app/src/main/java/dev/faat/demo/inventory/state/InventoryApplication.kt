package dev.faat.demo.inventory.state

import android.app.Application
import dev.faat.demo.inventory.data.AppContainer
import dev.faat.demo.inventory.data.AppDataContainer

class InventoryApplication : Application() {

    /**
     * AppContainer instance used by the rest of classes to obtain dependencies
     */
    lateinit var container: AppContainer

    override fun onCreate() {
        super.onCreate()
        container = AppDataContainer(this)
    }
}