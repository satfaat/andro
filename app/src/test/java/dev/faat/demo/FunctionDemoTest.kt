package dev.faat.demo

import org.junit.Test

class FunctionDemoTest {

    @Test
    fun function_vararg() {

        val greet = arrayOf("Hello", "Hallo", "Salut", "Hola", "你好")

        fun printAll(vararg messages: String) {
            for (m in messages) println(m)
        }
        printAll("Hello", "Hallo", "Salut", "Hola", "你好")
    }

    @Test
    fun function_pass_arr_as_vararg() {

        val greet = arrayOf("Hello", "Hallo", "Salut", "Hola", "你好")

        fun printAll(vararg messages: Array<String>) {
            for (m in messages) println(m)
        }
        printAll(greet) // as object
    }

    @Test
    fun function_pass_arr() {

        val greet = arrayOf("Hello", "Hallo", "Salut", "Hola", "你好")

        fun printAll(messages: Array<String>) {
            for (m in messages) println(m)
        }
        printAll(greet)
    }

    @Test
    fun function_with_prefix() {

        fun printAllWithPrefix(vararg messages: String, prefix: String) {
            for (m in messages) println(prefix + m)
        }
        printAllWithPrefix(
            "Hello", "Hallo", "Salut", "Hola", "你好",
            prefix = "Greeting: "
        )
    }

    @Test
    fun function_demo_vararg2() {

        fun printAll(vararg messages: String) {
            for (m in messages) println(m)
        }
        printAll("Hello", "Hallo", "Salut", "Hola", "你好")

        fun log(vararg entries: String) {
            printAll(*entries)
        }
        log("Hello", "Hallo", "Salut", "Hola", "你好")
    }
}