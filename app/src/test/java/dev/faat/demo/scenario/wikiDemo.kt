package dev.faat.demo.scenario

import com.microsoft.playwright.Page
import org.junit.Assert

fun shouldSearchWiki(page: Page) {
    page.navigate("https://www.wikipedia.org/")
    page.locator("input[name=\"search\"]").click()
    page.locator("input[name=\"search\"]").fill("playwright")
    page.locator("input[name=\"search\"]").press("Enter")
    Assert.assertEquals("https://en.wikipedia.org/wiki/Playwright", page.url())
}