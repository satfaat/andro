package dev.faat.demo

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserContext
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Page
import com.microsoft.playwright.Playwright
import dev.faat.demo.pages.SearchPage
import dev.faat.demo.scenario.shouldSearchWiki
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test


class PwDemoTest {
    companion object {
        // Shared between all tests in this class.
        var playwright: Playwright? = null
        var browser: Browser? = null

        // New instance for each test method.
        var context: BrowserContext? = null
        var page: Page? = null

        @JvmStatic
        @BeforeClass
        fun launchBrowser() {
            playwright = Playwright.create()
            browser = playwright!!.chromium()
                .launch(
                    BrowserType.LaunchOptions()
                        .setHeadless(false)
                )
        }

        @JvmStatic
        @AfterClass
        fun closeBrowser() {
            playwright!!.close()
        }
    }

    @Before
    fun createContextAndPage() {
        val context = browser?.newContext()
        if (context != null) {
            page = context.newPage()
        }
    }

    @After
    fun closeContext() {
        context?.close()
    }

    @Test
    fun shouldSearchWikiTest() {
        shouldSearchWiki(page!!)
    }

    @Test
    fun pomDemo() {
//        val page = browser!!.newPage()
        val searchPage = SearchPage(page!!)
        searchPage.navigate()
        searchPage.search("search query")
    }

//    fun shouldSearchWiki() {
//        page!!.navigate("https://www.wikipedia.org/")
//        page!!.locator("input[name=\"search\"]").click()
//        page!!.locator("input[name=\"search\"]").fill("playwright")
//        page!!.locator("input[name=\"search\"]").press("Enter")
//        assertEquals("https://en.wikipedia.org/wiki/Playwright", page!!.url())
//    }

//    @Test
//    fun `can load index`() {
//        page.navigate("http://localhost:$port")
//        assertThat(page.waitForSelector("h1").innerText()).contains("MailSlurp")
//    }

}