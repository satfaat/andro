package dev.faat.demo

import dev.faat.demo.inventory.data.InventoryDatabase
import dev.faat.demo.inventory.data.ItemDao
import org.junit.Test
import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
//@RunWith(AndroidJUnit4::class)
class ExampleUnitTest {
    private lateinit var itemDao: ItemDao
    private lateinit var db: InventoryDatabase

//    @Before
//    fun createDb() {
//        val context = ApplicationProvider.getApplicationContext<Context>()
//        db = Room.inMemoryDatabaseBuilder(
//            context, InventoryDatabase::class.java
//        ).build()
//        itemDao = db.itemDao()
//    }
//
//    @After
//    @Throws(IOException::class)
//    fun closeDb() {
//        db.close()
//    }


    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

//    @Test
//    @Throws(IOException::class)
//    fun db_demo() = runBlocking {
//        val item = Item(
//            1,
//            "Ivan",
//            40.2,
//            123,
//        )
//        itemDao.insert(item)
//        val row = itemDao.getItem(1)
//        println(row)
//    }

    @Test
    fun null_safety() {
        var neverNull: String = "This can't be null"
//        neverNull = null

        var nullable: String? = "You can keep a null here"
        nullable = null
        println(nullable)
    }

    @Test
    fun null_handle() {
        fun describeString(maybeString: String?): String {
            if (maybeString != null && maybeString.length > 0) {
                return "String of length ${maybeString.length}"
            } else {
                return "Empty or null string"
            }
        }
    }
}