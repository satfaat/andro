package dev.dem.interfacelib

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp


@Composable
fun BtnOutlined(
    @StringRes text: Int,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    OutlinedButton(
        modifier = modifier,
        onClick = onClick
    ) { Text(stringResource(text)) }
}

@Composable
fun Btn(
    @StringRes text: Int,
    onClick: () -> Unit,
    enabled: Boolean,
    modifier: Modifier = Modifier,
) {
    Button(
        modifier = modifier,
        // the button is enabled when the user makes a selection
        enabled = enabled,
        onClick = onClick
    ) {
        Text(stringResource(text))
    }
}


@Composable
fun Btn2(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Button(
        onClick = onClick,
        modifier
    ) {
        Text(text)
    }
}
