package dev.faat.compose2

import android.util.Log
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import dev.faat.compose2.ui.theme.DemoTheme
import dev.faat.compose2.view.nav.SootheBottomNavigation
import dev.faat.compose2.view.nav.SootheNavigationRail
import dev.faat.compose2.view.screen.HomeScreen
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass


@Composable
@Preview(showBackground = true, backgroundColor = 0xFFF5F0EE)
fun MySootheAppPortrait() {
    DemoTheme {
        Scaffold(
            bottomBar = { SootheBottomNavigation() }
        ) { padding ->
            HomeScreen(Modifier.padding(padding))
        }
    }
}

@Composable
@Preview(showBackground = true, backgroundColor = 0xFFF5F0EE, heightDp = 360, widthDp = 900)
fun MySootheAppLandscape() {
    DemoTheme {
        Surface(color = MaterialTheme.colorScheme.background) {
            Row {
                SootheNavigationRail()
                HomeScreen()
            }
        }
    }
}


@Composable
fun MySootheApp(windowSize: WindowSizeClass) {
    when (windowSize.widthSizeClass) {
        WindowWidthSizeClass.Compact -> {
            Log.d("MySootheApp", "isCompact: ${windowSize.widthSizeClass}")
            MySootheAppPortrait()
        }

        WindowWidthSizeClass.Expanded -> {
            Log.d("MySootheApp", "isExpanded: ${windowSize.widthSizeClass}")
            MySootheAppLandscape()
        }
        WindowWidthSizeClass.Medium -> {
            Log.d("MySootheApp", "isMedium: ${windowSize.widthSizeClass}")
            MySootheAppLandscape()
        }
    }
}