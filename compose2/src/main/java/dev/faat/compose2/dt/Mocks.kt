package dev.faat.compose2.dt

import dev.faat.compose2.R


val alignYourBodyData = listOf(
    R.drawable.ab1_inversions to R.string.ab1_inversions,
    R.drawable.ab1_inversions to R.string.ab2_quick_yoga,
    R.drawable.ab1_inversions to R.string.ab3_stretching,
    R.drawable.ab1_inversions to R.string.ab4_tabata,
    R.drawable.ab1_inversions to R.string.ab5_hiit,
    R.drawable.ab1_inversions to R.string.ab6_pre_natal_yoga
).map { DrawableStringPair(it.first, it.second) }


val favoriteCollectionsData = listOf(
    R.drawable.fc2_nature_meditations to R.string.fc2_nature_meditations,
    R.drawable.fc2_nature_meditations to R.string.fc2_nature_meditations,
    R.drawable.fc2_nature_meditations to R.string.fc2_nature_meditations,
    R.drawable.fc2_nature_meditations to R.string.fc2_nature_meditations,
    R.drawable.fc2_nature_meditations to R.string.fc2_nature_meditations,
    R.drawable.fc2_nature_meditations to R.string.fc2_nature_meditations
).map { DrawableStringPair(it.first, it.second) }