
## projects

- https://github.com/android/codelab-android-compose


## docu

- https://developer.android.com/develop/ui/compose/mental-model?authuser=1
- https://medium.com/androiddevelopers/consuming-flows-safely-in-jetpack-compose-cde014d0d5a3
- https://developer.android.com/develop/ui/compose/state#use-other-types-of-state-in-jetpack-compose
