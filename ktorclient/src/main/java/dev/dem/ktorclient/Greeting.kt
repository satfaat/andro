package dev.dem.ktorclient

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import dev.dem.ktorclient.ui.theme.DemoTheme
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText


class Greeting {

    private val client = HttpClient()

    suspend fun greet(): String {
        val response = client.get("https://ktor.io/docs/")
        return response.bodyAsText()
    }
}

@Composable
fun GreetingView(txt: String) {
    Text(txt)
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    DemoTheme {
        GreetingView("Ktor client")
    }
}
