package dev.faat.cupcake.nav

enum class RoutesList() {
    Start,
    Flavor,
    Pickup,
    Summary
}
