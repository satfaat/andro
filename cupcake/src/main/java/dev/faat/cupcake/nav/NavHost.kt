package dev.faat.cupcake.nav

import android.content.Context
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dev.faat.cupcake.R
import dev.faat.cupcake.dt.DataSource
import dev.faat.cupcake.dt.OrderUiState
import dev.faat.cupcake.state.OrderViewModel
import dev.faat.cupcake.ui.OrderSummaryScreen
import dev.faat.cupcake.ui.optionScreen.SelectOptionScreen
import dev.faat.cupcake.ui.makeOrderScreen.MakeOrderScreen


@Composable
fun NavHost0(
    viewModel: OrderViewModel = viewModel(),
    navController: NavHostController = rememberNavController(),
    uiState: OrderUiState,
    modifier: Modifier = Modifier,
    innerPadding: PaddingValues
) {
    NavHost(
        navController = navController,
        startDestination = RoutesList.Start.name,
        modifier = modifier.padding(innerPadding)
    ) {
        composable(route = RoutesList.Start.name) {
            MakeOrderScreen(
                quantityOptions = DataSource.quantityOptions,
                {},
                modifier = Modifier
                    .fillMaxSize()
                    .padding(dimensionResource(R.dimen.padding_medium))
            )
        }
        composable(route = RoutesList.Flavor.name) {
            val context: Context = LocalContext.current
            SelectOptionScreen(
                subtotal = uiState.price,
                onNextButtonClicked = { navController.navigate(RoutesList.Pickup.name) },
                onCancelButtonClicked = {},
                options = DataSource.flavors.map { id -> context.resources.getString(id) },
                onSelectionChanged = { viewModel.setFlavor(it) },
                modifier = Modifier.fillMaxHeight()
            )
        }
        composable(route = RoutesList.Pickup.name) {
            SelectOptionScreen(
                subtotal = uiState.price,
                onNextButtonClicked = { navController.navigate(RoutesList.Summary.name) },
                onCancelButtonClicked = {},
                options = uiState.pickupOptions,
                onSelectionChanged = { viewModel.setDate(it) },
                modifier = Modifier.fillMaxHeight()
            )
        }
        composable(route = RoutesList.Summary.name) {
            val context = LocalContext.current
            OrderSummaryScreen(
                orderUiState = uiState,
                onCancelButtonClicked = {},
                onSendButtonClicked = { subject: String, summary: String ->
                    shareOrder(context, subject = subject, summary = summary)
                },
                modifier = Modifier.fillMaxHeight()
            )
        }
    }
}


@Composable
@Preview(showBackground = true)
fun NavHost0Preview() {
    NavHost0(innerPadding = PaddingValues(), uiState = OrderUiState())
}