package dev.faat.cupcake.ui.makeOrderScreen

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier


@Composable
fun OptionsContent(
    quantityOptions: List<Pair<Int, Int>>,
    onNextButtonClicked: (Int) -> Unit,
    modifier: Modifier
) {
    quantityOptions.forEach { item ->
        SelectQuantityButton(
            labelResourceId = item.first,
            onClick = { onNextButtonClicked(item.second) },
            modifier = modifier.fillMaxWidth(),
        )
    }
}