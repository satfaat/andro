package dev.faat.syntax.di


class Engine {
    fun start() {
        println("start engine")
    }
}

class Car {
    private val engine = Engine()
    fun start() {
        engine.start()
    }
}

class Car2(private val engine: Engine) {
    fun start() {
        engine.start()
    }
}


fun main() {
    val engine = Engine()
    val car = Car2(engine)
    car.start()
}