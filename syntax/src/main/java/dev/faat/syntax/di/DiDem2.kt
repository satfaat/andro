package dev.faat.syntax.di


object ServiceLocator {
    fun getEngine(): Engine = Engine()
}

private class Car3 {
    private val engine = ServiceLocator.getEngine()

    fun start() {
        engine.start()
    }
}

fun main() {
    val car = Car()
    car.start()
}