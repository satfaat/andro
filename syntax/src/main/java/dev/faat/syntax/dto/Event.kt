package dev.faat.syntax.dto

import java.time.LocalDate

sealed class Event {
    abstract val id: EventId
    abstract val title: Title
    abstract val organizer: Organizer
    abstract val description: Description
    abstract val ageRestriction: AgeRestriction
    abstract val date: LocalDate

    data class Online(
        override val id: EventId,
        override val title: Title,
        override val organizer: Organizer,
        override val description: Description,
        override val ageRestriction: AgeRestriction,
        override val date: LocalDate,
        val url: Url
    ) : Event()

    data class AtAddress(
        override val id: EventId,
        override val title: Title,
        override val organizer: Organizer,
        override val description: Description,
        override val ageRestriction: AgeRestriction,
        override val date: LocalDate,
        val address: Address
    ) : Event()
}