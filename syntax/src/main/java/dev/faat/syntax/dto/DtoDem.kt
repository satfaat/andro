package dev.faat.syntax.dto

import java.time.LocalDate


@JvmInline
value class EventId(val value: Long)

@JvmInline
value class Organizer(val value: String)

@JvmInline
value class Title(val value: String)

@JvmInline
value class Description(val value: String)

@JvmInline
value class Url(val value: String)

@JvmInline
value class City(val value: String)

@JvmInline
value class Street(val value: String)

data class Address(val city: City, val street: Street)

data class Event1(
    val id: EventId,
    val title: Title,
    val organizer: Organizer,
    val description: Description,
    val date: LocalDate,
    val ageRestriction: AgeRestriction,
    val isOnline: Boolean,
    val url: Url?,
    val address: Address?
)

enum class AgeRestriction(val description: String) {
    General("All ages admitted. Nothing that would offend parents for viewing by children."),
    PG("Some material may not be suitable for children. Parents urged to give \"parental guidance\""),
    PG13("Some material may be inappropriate for children under 13. Parents are urged to be cautious."),
    Restricted("Under 17 requires accompanying parent or adult guardian. Contains some adult material."),
    NC17("No One 17 and Under Admitted. Clearly adult.")
}

fun printLocation(event: Event): Unit =
    when (event) {
        is Event.Online -> println(event.url.value)
        is Event.AtAddress -> println("${event.address.city}: ${event.address.street}")
    }

fun main() {
    val e = Event.Online(
        EventId(0L),
        Title("Functional Domain Modeling"),
        Organizer("Simon Vergauwen"),
        Description("In this blogpost we dive into functional DDD..."),
        AgeRestriction.General,
        LocalDate.now(),
        Url("super.com"),
    )

    println(e)
    println(e.id.value)

    println(AgeRestriction.PG)
    println(AgeRestriction.PG.description)
}