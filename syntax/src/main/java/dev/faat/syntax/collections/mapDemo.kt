package dev.faat.syntax.collections

// unique keys are mapped to other values


val solarSystemMap = mutableMapOf(
    "Mercury" to 0,
    "Venus" to 0,
    "Earth" to 1,
    "Mars" to 2,
    "Jupiter" to 79,
    "Saturn" to 82,
    "Uranus" to 27,
    "Neptune" to 14
)


fun main(){
    println(solarSystemMap.size)

    // add key-value to map
    solarSystemMap["Pluto"] = 5

    println(solarSystemMap.size)

    // get value
    println(solarSystemMap["Pluto"])
    println(solarSystemMap.get("Theia"))

    solarSystemMap.remove("Pluto")

    // update value
    println(solarSystemMap["Jupiter"])
    solarSystemMap["Jupiter"] = 78
    println(solarSystemMap["Jupiter"])
}