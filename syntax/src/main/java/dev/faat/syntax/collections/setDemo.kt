package dev.faat.syntax.collections

// does not have a specific order
// does not allow duplicate values
// unique

val id1 = "Faat".hashCode()
val id2 = "Faat!".hashCode()

val solarSystemSet =
    mutableSetOf("Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune")

fun main() {
    println(id1)
    println(id2)

    println(solarSystemSet.size)

    // Elements in sets don't necessarily have an order, so there's no index
    solarSystemSet.add("Pluto")

    println(solarSystemSet.contains("Pluto")) // true or false

    solarSystemSet.remove("Pluto")
}