package dev.faat.syntax.collections

// is the simplest way to group an arbitrary number of values in your programs
// ordered
// are accessed with an index
// fixed size of array

val rockPlanets = arrayOf<String>("Mercury", "Venus", "Earth", "Mars")
val gasPlanets = arrayOf("Jupiter", "Saturn", "Uranus", "Neptune")
val solarSystem = rockPlanets + gasPlanets

val newSolarSystem = arrayOf("Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto")

fun main() {
    // Access an element in an array
    println(solarSystem[3])

    // set the value of an array element by its index
    solarSystem[3] = "Little Earth"
    println(solarSystem[3])

//    solarSystem[8] = "Pluto"
}