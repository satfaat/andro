package dev.faat.syntax.collections

// is an ordered,
// resizable
// can also insert new elements between other elements
// List is an interface that defines properties and methods related to a read-only ordered collection of items.
// MutableList extends the List interface by defining methods to modify a list, such as adding and removing elements

val solarSystemLs =
    listOf("Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune")
val solarSystemVar =
    mutableListOf("Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune")

fun main() {
    println(solarSystemLs.size)

    // Access elements from a list
    println(solarSystemLs[2])
    println(solarSystemLs.get(3))

    println(solarSystemLs.indexOf("Earth"))
    println(solarSystemLs.indexOf("Pluto"))

    // Iterate over list elements using a for loop
    for (planet in solarSystemLs) {
        println(planet)
    }

    // Add elements to a list
    solarSystemVar.add("Pluto")
    solarSystemVar.add(3, "Theia")

    // Update elements at a specific index
    solarSystemVar[3] = "Future Moon"

    // Remove elements from a list
    solarSystemVar.removeAt(9)
    solarSystemVar.remove("Future Moon")

    println(solarSystemVar.contains("Pluto"))  // return true or false
    println("Future Moon" in solarSystemVar) // return true or false
}