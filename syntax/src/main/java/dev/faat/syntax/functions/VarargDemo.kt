package dev.faat.syntax.functions

class VarargDemo {
}


fun manyParams(vararg str: String){
    println(str.joinToString(" "))
}

fun main(){
    manyParams("Home", "Pay", "Sky")
}