package dev.faat.syntax.functions


fun daysToString(days: Int? = null) {
    days?.let {
        days.toString().also {
            println(it)
        }
    }
}


fun main() {
    daysToString(365)
    println("Null:")
    daysToString()
}



