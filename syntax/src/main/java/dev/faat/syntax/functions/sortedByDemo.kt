package dev.faat.syntax.functions


val alphabeticalMenu = cookies.sortedBy {
    it.name
}


fun main() {
    println("Alphabetical menu:")
    alphabeticalMenu.forEach {
        println(it.name)
    }
}