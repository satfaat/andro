package dev.faat.syntax.functions

// When a function returns a function or takes a function as an argument, it's called a higher-order function

fun main() {
    // to refer to a function as a value
    val trickFunction = ::trick

    //now refers to a variable, rather than a function name
    val trickFunctionLambda = trickLambda
    trickFunctionLambda()
    treat()

    val treatFunction = trickOrTreat(false)
    val trickFunctionLambda2 = trickOrTreat(true)
    treatFunction()
    trickFunctionLambda2()

    val treatFunction2 = trickOrTreatParam(false, coins)
    val trickFunctionLambda3 = trickOrTreatParam(true, cupcake)
    treatFunction2()
    trickFunctionLambda3()

    val treatFunction3 = trickOrTreatNull(false, coins)
    val trickFunctionLambda4 = trickOrTreatNull(true, null)
    treatFunction3()
    trickFunctionLambda4()

    // Pass a lambda expression directly into a function
    val treatFunction5 = trickOrTreatNull(false, { "$it quarters" })
    // Use trailing lambda syntax
//    val treatFunction5 = trickOrTreat(false) { "$it quarters" }
    val trickFunctionLambda5 = trickOrTreatNull(true, null)
    repeat(4) {
        treatFunction5()
    }
    trickFunctionLambda5()
}


fun trick() {
    println("No treats!")
}

//define a function with a lambda expression
val trickLambda = {
    println("No treats!")
}

// Use functions as a data type
val treat: () -> Unit = {
    println("Have a treat!")
}

// Use a function as a return type
fun trickOrTreat(isTrick: Boolean): () -> Unit {
    if (isTrick) {
        return trickLambda
    } else {
        return treat
    }
}


// Pass a function to another function as an argument
fun trickOrTreatParam(isTrick: Boolean, extraTreat: (Int) -> String): () -> Unit {
    if (isTrick) {
        return trickLambda
    } else {
        println(extraTreat(5))
        return treat
    }
}

//val coins: (Int) -> String = { quantity ->
//    "$quantity quarters"
//}

// Omit parameter name, shorthand
val coins: (Int) -> String = {
    "$it quarters"
}

val cupcake: (Int) -> String = {
    "Have a cupcake!"
}

// Nullable function types
fun trickOrTreatNull(isTrick: Boolean, extraTreat: ((Int) -> String)?): () -> Unit {
    if (isTrick) {
        return trickLambda
    } else {
        if (extraTreat != null) {
            println(extraTreat(5))
        }
        return treat
    }
}
