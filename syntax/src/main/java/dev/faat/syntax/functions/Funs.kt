package dev.faat.syntax.functions

val opo = "new"

val short : () -> Unit = { println("short fun")}

val short2 = { println("short 2")}

// lambda
val a = { i: Int -> i + 1 }
val sum: (Int, Int) -> Int = { x: Int, y: Int -> x + y }

val repeatFun: String.(Int) -> String = { times -> this.repeat(times) }


fun main(){
    short()
    short2()
    println(opo::class) // find-out type
    println(short2::class)

    println(a(2))
    println("dog".repeatFun(2))
}