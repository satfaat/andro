package dev.faat.syntax.functions

// function is used to generate a single value from a collection
// like reduce(), sum(), sumOf()


val totalPrice = cookies.fold(0.0) { total, cookie ->
    total + cookie.price
}


fun main() {
    println("Total price: $$totalPrice")
}