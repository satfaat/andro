package dev.faat.syntax.functions

// map() could transform a List<Cookie> into a List<String>

val fullMenu = cookies.map {
    "${it.name} - $${it.price}"
}

val strWithNumber = { stop: Int ->
    (1 until stop)
        .map { "tr.tablerow:nth-child($it) > td:nth-child(1)" }
        .toList()
}

fun main() {
    println("Full menu:")
    fullMenu.forEach {
        println(it)
    }

    println(strWithNumber(6))
}