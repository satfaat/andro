package dev.faat.syntax.functions

// The filter() function lets you create a subset of a collection. For example,
// if you had a list of numbers,
// you could use filter() to create a new list that only contains numbers divisible by 2


val softBakedMenu = cookies.filter {
    it.softBaked
}


fun main() {
    println("Soft cookies:")
    softBakedMenu.forEach {
        println("${it.name} - $${it.price}")
    }
}