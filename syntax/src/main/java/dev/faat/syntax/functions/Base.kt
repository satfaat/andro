package dev.faat.syntax.functions

fun foo() {
    println("foo called")
    Thread.sleep(1000)

    throw RuntimeException("Foo stuck")
}


fun exceptionHandler(fn: () -> Unit) {
    try {
        fn()
    } catch (ex: Exception) {
        val stackTrace = ex.stackTrace
        println(stackTrace[0])
        println(stackTrace[1])
        println("***")

        stackTrace.forEach() {
            println(it)
        }
    }
}

fun main() {
    exceptionHandler(::foo)
}