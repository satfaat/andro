package dev.faat.syntax.functions

// can be used to turn a list into a map
// Each unique return value of the function becomes a key in the resulting map
// list > groupBy() > map

val groupedMenu = cookies.groupBy { it.softBaked }

val softBakedMenuMap = groupedMenu[true] ?: listOf()
val crunchyMenu = groupedMenu[false] ?: listOf()

fun main() {
    println("Soft cookies:")
    softBakedMenu.forEach {
        println("${it.name} - $${it.price}")
    }

    println("Crunchy cookies:")
    crunchyMenu.forEach {
        println("${it.name} - $${it.price}")
    }
}