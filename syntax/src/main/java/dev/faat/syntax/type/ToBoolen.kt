package dev.faat.syntax.type


fun isTrueOrFalse(): Boolean {
    val input: String? = readLine()
    return (input?.toInt() == 1)
}


fun main() {

//    val input = readLine()
//    println("Get string: $input")
    println("1".toBoolean())
    println(null.toBoolean())
    println("true".toBoolean())
//    val isTrueOrFalse = input.toBoolean()
//    println(isTrueOrFalse)
    println(isTrueOrFalse())

    println("***")
    println("hi" == "hi")
    println("hi" == "Hi")
    println("hi" == null)
    println(null == null)
}