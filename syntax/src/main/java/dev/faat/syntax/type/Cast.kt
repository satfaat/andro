package dev.faat.syntax.type


val intToStr = (2584).toString()

val price = 2000.25010
val priseAsStr = "2,125.00"
val formattedPrice1 = "%,.0f".format(price)
val formattedPrice2 = "%,.0f".format(price).replace(",", " ")

fun main() {
    println((1236).toString())
    println(intToStr::class)

    println("\n")
    println(price.toString().replace(".", " "))
    println(formattedPrice1)
    println(formattedPrice2)

//    try {
//        (priseAsStr.toDouble())
//    } catch (e: Error) {
//        e.message
//    }

    try {
        (priseAsStr.toDouble())
    } catch (e: Exception) {
        println(e.message)
    }
    println(priseAsStr.replace(",", "").toDouble())
}