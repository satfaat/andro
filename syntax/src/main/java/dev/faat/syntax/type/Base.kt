package dev.faat.syntax.type


val greet = "some greet"

val message = StringBuilder("Hello ")
val update = message.append("there")

fun main() {
    println(greet)
    println(greet::class)
    println(greet.javaClass)

    println(update)
}