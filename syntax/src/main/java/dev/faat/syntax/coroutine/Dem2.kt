package dev.faat.syntax.coroutine

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.yield



suspend fun tagOut() {
    println("    Tagging out!    ")
    yield()
}

//suspend fun getExample(): String {
//    return client.get("https://www.example.com/").bodyAsText()
//}

fun main() {
    runBlocking {
        launch {
            println("Hammer: Clothesline!")
            yield()
            println("Hammer: Piledriver!")
            yield()
        }
        println("Sledge: Suplex!")
        tagOut()
        println("Sledge: Figure-four Leglock!")
        tagOut()
        println("Sledge: Pinning 1-2-3!")
    }
}