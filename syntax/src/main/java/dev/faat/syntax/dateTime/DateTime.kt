package dev.faat.syntax.dateTime

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DateUtils {

    companion object {

        fun getFormattedDateBeforeToday(days: Long, formatPattern: String): String {
            val today = LocalDate.now()
            val dateBeforeToday = today.minusDays(days)
            val formatter = DateTimeFormatter.ofPattern(formatPattern)
            return dateBeforeToday.format(formatter)
        }
    }
}


fun main() {
    val today = LocalDate.now() // Get today's date
    val thirtyDaysAgo = today.minusDays(30) // Subtract 30 days

    val formattedDate =
        thirtyDaysAgo.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) // Format the date

    println(today)
    println(thirtyDaysAgo)
    println(formattedDate)
    println("//")
    println(DateUtils.getFormattedDateBeforeToday(30, "dd.MM.yyyy"))
}