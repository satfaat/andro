package dev.faat.syntax.regex

import java.util.regex.Pattern

class RegExDemo {
}


fun main() {
    println(Pattern.compile("Welcome, .*"))

    val regex = Pattern.compile("\\d+")
    val matcher = regex.matcher("Hello 123 World 456")

    while (matcher.find()) {
        println("Match found: ${matcher.group()}")
    }
}