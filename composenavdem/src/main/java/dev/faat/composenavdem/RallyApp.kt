package dev.faat.composenavdem

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import dev.faat.composenavdem.ui.components.RallyTabRow
import dev.faat.composenavdem.ui.theme.DemoTheme
import dev.faat.composenavdem.view.accounts.AccountsScreen
import dev.faat.composenavdem.view.accounts.SingleAccountScreen
import dev.faat.composenavdem.view.bills.BillsScreen
import dev.faat.composenavdem.view.overview.OverviewScreen

@Composable
@Preview
fun RallyApp() {
    DemoTheme {

        val navController = rememberNavController()
        val currentBackStack by navController.currentBackStackEntryAsState()
        val currentDestination = currentBackStack?.destination
        val currentScreen =
            rallyTabRowScreens.find { it.route == currentDestination?.route } ?: Overview
        Scaffold(
            topBar = {
                RallyTabRow(
                    allScreens = rallyTabRowScreens,
                    onTabSelected = { newScreen -> navController.navigateSingleTopTo(newScreen.route) },
                    currentScreen = currentScreen
                )
            }
        ) { innerPadding ->
            NavHost(
                navController = navController,
                startDestination = Overview.route,
                Modifier.padding(innerPadding)
            ) {
                composable(route = Overview.route) {
                    OverviewScreen(
                        onClickSeeAllAccounts = { navController.navigateSingleTopTo(Accounts.route) },
                        onClickSeeAllBills = { navController.navigateSingleTopTo(Bills.route) },
                        onAccountClick = { accountType ->
                            navController.navigateSingleTopTo("${SingleAccount.route}/$accountType")
                        }
                    )
                }
                composable(route = Accounts.route) {
                    AccountsScreen(
                        onAccountClick = { accountType ->
                            navController.navigateSingleTopTo("${SingleAccount.route}/$accountType")
                        }
                    )
                }
                composable(route = Bills.route) {
                    BillsScreen()
                }
                composable(
                    route = SingleAccount.routeWithArgs,
                    arguments = SingleAccount.arguments,
                    deepLinks = SingleAccount.deepLinks
                ) { navBackStackEntry ->
                    // Retrieve the passed argument
                    val accountType =
                        navBackStackEntry.arguments?.getString(SingleAccount.accountTypeArg)
                    // Pass accountType to SingleAccountScreen
                    SingleAccountScreen(accountType)
                }
            }
        }
    }
}

fun NavHostController.navigateSingleTopTo(route: String) =
    this.navigate(route) {
        popUpTo(this@navigateSingleTopTo.graph.findStartDestination().id) { saveState = true }
        launchSingleTop = true
        restoreState = true
    }

private fun NavHostController.navigateToSingleAccount(accountType: String) {
    this.navigateSingleTopTo("${SingleAccount.route}/$accountType")
}