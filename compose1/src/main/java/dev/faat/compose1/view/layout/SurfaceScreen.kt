package dev.faat.compose1.view.layout

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.faat.compose1.nav.router.BackButtonHandler
import dev.faat.compose1.nav.router.JetFundamentalsRouter
import dev.faat.compose1.nav.router.Screen

@Composable
fun SurfaceScreen(modifier: Modifier) {

    Box(modifier.fillMaxSize()) {
        MySurface(modifier.align(Alignment.Center))
    }

    BackButtonHandler {
        JetFundamentalsRouter.navigateTo(Screen.Navigation)
    }
}

@Composable
fun MySurface(modifier: Modifier) {
    Surface(
        modifier = modifier.size(100.dp),
        color = Color.DarkGray,
        contentColor = Color.LightGray,
        tonalElevation = 10.dp,
        border = BorderStroke(1.dp, Color.Black),
        content = { MyColumn() }
    )
}

@Preview
@Composable
fun MySurfacePreview() {
    MySurface(modifier = Modifier)
}