package dev.faat.compose1.view.scaffold

import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.tooling.preview.Preview


@Composable
@Preview
fun ScaffoldExample() {
    var presses by remember { mutableIntStateOf(0) }

    Scaffold(
        topBar = { TopAppbar() },
        bottomBar = { BottomAppbar() },
        floatingActionButton = { ActionButton({ presses++ }) },
    ) { innerPadding ->
        BarContent(innerPadding, presses)
    }
}