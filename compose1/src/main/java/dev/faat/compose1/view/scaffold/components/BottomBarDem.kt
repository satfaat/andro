package dev.faat.compose1.view.scaffold.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.BottomAppBarDefaults
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview


@Composable
@Preview
fun BottomBar() {
    BottomAppBar(
        actions = {
            NewIcon(
                { /*TODO*/ },
                Icons.Filled.Check,
                "Check"
            )
            NewIcon(
                { /*TODO*/ },
                Icons.Filled.Edit,
                "Edit"
            )
            NewIcon(
                { /*TODO*/ },
                img = Icons.Filled.Email,
                contentDescription = "Email"
            )
            NewIcon(
                { /*TODO*/ },
                img = Icons.Filled.Settings,
                contentDescription = "Settings"
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = { /*TODO*/ },
                containerColor = BottomAppBarDefaults.bottomAppBarFabColor,
                elevation = FloatingActionButtonDefaults.bottomAppBarFabElevation()
            ) {
                Icon(Icons.Filled.Add, "Localized description")
            }
        }
    )
}


@Composable
fun NewIcon(onClick: () -> Unit, img: ImageVector, contentDescription: String) {
    IconButton(onClick) {
        Icon(
            img,
            contentDescription = contentDescription
        )
    }
}

@Composable
fun BarContent(innerPadding: PaddingValues) {
    Text(
        modifier = Modifier.padding(innerPadding),
        text = "Example of a scaffold with a bottom app bar."
    )
}