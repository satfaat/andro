package dev.faat.compose1

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import dev.faat.compose1.greet.view.GreetingFragment
import dev.faat.compose1.ui.theme.DemoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            DemoTheme {
//                JetFundamentalsApp()
//                ScaffoldExample()
                GreetingFragment(modifier = Modifier.fillMaxSize())
            }
        }
    }
}