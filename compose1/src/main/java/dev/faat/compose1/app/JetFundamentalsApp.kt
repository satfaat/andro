package dev.faat.compose1.app

import androidx.compose.animation.Crossfade
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import dev.faat.compose1.nav.router.JetFundamentalsRouter
import dev.faat.compose1.nav.router.Screen
import dev.faat.compose1.view.AlertDialogScreen
import dev.faat.compose1.view.ExploreButtonsScreen
import dev.faat.compose1.nav.NavigationScreen
import dev.faat.compose1.view.ProgressIndicatorScreen
import dev.faat.compose1.view.TextFieldScreen
import dev.faat.compose1.view.TextScreen
import dev.faat.compose1.view.layout.BoxScreen
import dev.faat.compose1.view.layout.ColumnScreen
import dev.faat.compose1.view.layout.RowScreen
import dev.faat.compose1.view.layout.ScaffoldScreen
import dev.faat.compose1.view.layout.SurfaceScreen

@Composable
fun JetFundamentalsApp() {
    Surface(color = MaterialTheme.colorScheme.background) {
        Crossfade(targetState = JetFundamentalsRouter.currentScreen) { screenState ->
            when (screenState.value) {
                is Screen.Row -> RowScreen()
                is Screen.Column -> ColumnScreen()
                is Screen.Box -> BoxScreen()
                is Screen.Surface -> SurfaceScreen(Modifier)
                is Screen.Scaffold -> ScaffoldScreen()
                is Screen.Navigation -> NavigationScreen()
                is Screen.Text -> TextScreen()
                is Screen.TextField -> TextFieldScreen()
                is Screen.Buttons -> ExploreButtonsScreen()
                is Screen.ProgressIndicator -> ProgressIndicatorScreen()
                is Screen.AlertDialog -> AlertDialogScreen()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun AppPreview() {
    JetFundamentalsApp()
}