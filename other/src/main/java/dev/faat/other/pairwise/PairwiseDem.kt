package dev.faat.other.pairwise

import kotlinx.serialization.json.Json
import kotlin.math.min


fun generatePairwiseCombinations(lists: List<List<String>>): List<List<String>> {
    val pairs = mutableListOf<List<String>>()
    val maxLength = lists.maxOf { it.size }

    for (i in 0 until maxLength) {
        val pair = lists.map { it[min(i, it.size - 1)] }
        pairs.add(pair)
    }

    return pairs
}


fun main() {

    val parameters = Json.decodeFromString<Parameters>(jsonData)
    println(parameters)

    val parameterLists = listOf(
        parameters.Brand,
        parameters.OS,
        parameters.Connection,
        parameters.Employment,
        parameters.Duration.map { it.toString() }
    )
    println(parameterLists)

    val pairs = generatePairwiseCombinations(parameterLists)
    pairs.forEachIndexed { index, pair ->
        println("${index + 1}: $pair")
    }
}