package dev.faat.other.pairwise

import kotlinx.serialization.Serializable


@Serializable
data class Parameters(
    val Brand: List<String>,
    val OS: List<String>,
    val Connection: List<String>,
    val Employment: List<String>,
    val Duration: List<Int>
)

val jsonData = """
        {
            "Brand": ["Brand X", "Brand Y"],
            "OS": ["98", "NT", "2000", "XP"],
            "Connection": ["Internal", "Modem"],
            "Employment": ["Salaried", "Hourly", "Part-Time", "Contract"],
            "Duration": [6, 10, 15, 30, 60]
        }
    """