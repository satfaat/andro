package dev.faat.other

import java.io.File

val path = "C:\\fs\\notes\\db.txt"

fun heidipass(code: String): String {
    val ascii = code.dropLast(1)
    val d = code.last().toString().toInt()
    val decode: (String) -> Char = { x -> (x.toInt(16) - d).toChar() }
    return ascii.chunked(2).map(decode).joinToString("")
}

fun decodeHeidi(cipher: String): String {
    val d: Int = cipher.last().digitToInt()  // Char to Int
    return cipher.dropLast(1)
        .chunked(2) // String to List<String>
        .map { (it.toInt(16) - d).toChar() }  // decode to List<Char>
        .joinToString("")
}


fun getPw(path: String) {
    val file: File = File(path) // Profile saved location
    val lines: List<String> = file.readLines().filter { "\\Password<" in it }
    val passwords: List<String> = lines.map { it.split("<|||>").last() }

    passwords.forEach { println(heidipass(it)) }
}

fun getPwList(path: String): List<String> {
    return File(path).readLines()
        .filter { "\\Password<" in it }
        .map { it.split("<|||>").last() }
}

fun filterForPwDem() {
    // Get file and filter for pw
    val file: File = File(path)
    val lines =
        println(file) //C:\fs\notes\db.txt
//    println(file.readText()) // content
//    println(file.readLines()) // lines to List<String>
    println(file.readLines().filter { "\\Password<" in it }) // filter
    println(
        file.readLines()
            .filter { "\\Password<" in it }
            .map { it.split("<|||>") }
    )
    println(
        file.readLines()
            .filter { "\\Password<" in it }
            .map { it.split("<|||>").last() }
    )
}

fun decodeDem() {
    val x = "34523462456241234"
        .dropLast(1)
    val chunk = x.chunked(2)


    // Decode word
    println(
        "34523462456241234"
            .dropLast(1)
            .last().digitToInt()  // Char to Int
    )

    println(
        "34523462456241234"
            .chunked(2) // String to List<String>
    )

    println(
        "34523462456241234"
            .chunked(2) // String to List<String>
            .map { (it.toInt(16) - 5).toChar() }
            .joinToString("")
    )
}

fun main() {

    getPwList(path).forEach { println(decodeHeidi(it)) }
}